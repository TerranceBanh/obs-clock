const count1 = (n1, n2) => {
  counters[n1] = 0
  counters[n2]++
}

 const handMove = [
  (n) => {
    hand[n].style.transform = `
      rotate(${
        (counters[0] + (time[0] * counters[1])) * (360 / time[1] / time[0])
      }deg)
      translateY(var(--handH1-1))
    `
  },
  (n) => {
    hand[n].style.transform = `
      rotate(${
        (counters[0] + (time[0] * counters[1]) + (time[0] * time[1] * counters[2])) * (360 / time[2] / time[1] / time[0])
      }deg)
      translateY(var(--handH1-1))
    `
  },
  (n) => {
    hand[n].style.transform = `
      rotate(${
        (
          counters[0] + 
          (time[0] * counters[1]) + 
          (time[0] * time[1] * counters[2]) +  
          (time[0] * time[1] * time[2] * counters[3])
        ) * 
        (360 / time[3] / time[2] / time[1] / time[0])
      }deg)
      translateY(var(--handH1-1))
    `
  },
] 

// On Click OR On Load
window.addEventListener('load', () => {
  if (running) {
    clearInterval(interval)
    running = !running
    return
  } 
  else running = !running
  interval = setInterval(() => {
    counters[0]++
    handMove[0](0)
    handMove[1](1)
    handMove[2](2)
    if (counters[0] >= time[0]) { // 100% Centiseconds
      count1(0, 1)
      if (counters[1] >= time[1]) { // 100% Seconds
        count1(1, 2)
        if (counters[2] >= time[2]) { // 100% Minutes
          count1(2, 3)
          if (counters[3] >= time[3]) { // 100% Hours
            counters[3] = 0
          }
        }
      }
    }
  },5)
})
