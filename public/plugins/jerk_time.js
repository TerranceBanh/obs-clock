const count = (n1, n2) => {
  hand[n1].style.transform = `
    rotate(${(counters[n2]) * (360 / time[n1 + 1])}deg)
    translateY(calc((var(--handH1-${n1 + 1}) * var(--scale)) + (var(--handBW-${n1 + 1}) * var(--scale))))
  `
}

const desyncTolerance = 50

// On Click Or On Load
window.addEventListener('load', () => {

  const t = () => [
    new Date().getMilliseconds(),
    new Date().getSeconds(),
    new Date().getMinutes(),
    new Date().getHours(),
  ]
  
  t().map((a,b) => counters[b] = a)
  hand.map((a,b) => count(b, b+1))

  const sync = () => {
    setTimeout(() => { 
      interval = setInterval(() => {

        t().map((a,b) => counters[b] = a)
        hand.map((a,b) => count(b, b+1))

        if(counters[0] >= (desyncTolerance < 2 ? 2 : desyncTolerance)) {
          clearInterval(interval)
          sync()
        }

      }, 1000)
    }, 1000 - counters[0])
  }
  sync()
})
