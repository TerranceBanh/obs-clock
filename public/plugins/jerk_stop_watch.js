const count = (n1, n2) => {
  hand[n1].style.transform = `
    rotate(${(counters[n2] + 1) * (360 / time[n1 + 1])}deg)
    translateY(calc((var(--handH1-${n1 + 1}) * var(--scale)) + (var(--handBW-${n1 + 1}) * var(--scale))))
  `
  counters[n1] = 0
  counters[n2]++
}

// On Click Or On Load
window.addEventListener('load', () => {
  if (running) {
    clearInterval(interval)
    running = !running
    return
  } 
  else running = !running
  interval = setInterval(() => {
    counters[0]++
    if (counters[0] >= time[0]) { // 100% Centiseconds
      count(0, 1)
      if (counters[1] >= time[1]) { // 100% Seconds
        count(1, 2)
        if (counters[2] >= time[2]) { // 100% Minutes
          count(2, 3)
          if (counters[3] >= time[3]) { // 100% Hours
            counters[3] = 0
          }
        }
      }
    }
  },5)
})
