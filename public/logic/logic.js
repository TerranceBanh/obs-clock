'use strict'
let hands = ['secHand', 'minHand', 'hourHand'] // Remove any of these to remove hands on clock

for (let i = 0; i < hands.length; i++) {
  const div = document.createElement('div')
  div.classList = hands[i]
  container.appendChild(div)
}

const hours = 12 // Change between 12 and 24 hours
for (let i = 0; i < hours; i++) {
  const div = document.createElement('div')
  div.classList = `hour`
  div.style.transform = `
    rotate(${(i + 1) * (360 / hours)}deg) 
    translateY(calc(
      (var(--diam) * var(--scale) / -2) + 
      (var(--hourH) * var(--scale) / 2) +
      var(--hourBW) * var(--scale) -
      var(--hourOffset) * var(--scale)
    ))
  `
  container.appendChild(div)
}

const hand = [
  document.querySelector('.secHand'),
  document.querySelector('.minHand'),
  document.querySelector('.hourHand'),
]

let running = false
let interval
const counters = [0,0,0,0]

const time = [
  200, // 2x Centiseconds => Second
  60, // Seconds => Minute
  60, // Minutes => Hour
  hours // Hour => Day
]

/* PLUGIN CODE AFTER THIS POINT */
